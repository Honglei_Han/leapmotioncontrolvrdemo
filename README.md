# Brief introduction of interaction method in the game

- More information, please feel free to visit my personal website [here](https://hanhonglei.github.io/).

There are two methods to play this game. One is traditional method, using keyboard and mouse. The other one is using VR glasses and leap motion hand controller.

1.	Tradition method

	Open the scene named “VRLeapmotion” in folder “Scenes”

	When debug play, use “Enter” to start a wave. The game purpose is to stop the enemies from arriving to destination point.

	When debug play, can use mouse left button and middle wheel to control the weapon. Mouse left button to fire, and middle wheel to change weapon

	You can also use arrow left, right, up, down to rotate the camera. Use keyboard Z and X to zoom in and out.

2.	VR leap motion method

	When leap motion is equipped, you can use your two hands in VR mounted mode to interact with the game. 

	You can use right hand to control camera. Five fingers extended, and palm slider right, left, up, or down to rotate camera.

	Right hand fist can zoom camera.

	Left hand palm towards the camera and keeping five fingers extended can call out the menu. And can use right hand to pinch these items.

	Right hand thumb up can start an enemy wave.

## Tips

	When you upgraded unity to version 5.5, may appear an error on leap motion script. Like this “Extension method must be defined in a non-generic static class”
If you remove "this" from the parameter, that should resolve the VS error. Please check [here](https://community.leapmotion.com/t/imported-hands-module-error-in-visual-studio-from-riggedhand-class/5221)

	Sometimes, when some unity internal errors still appear, you may need reimport all in Assets menu.

	You need to download `Oculus Rift SDK`, and Leap motion [SDK packages for Unity](https://developer.leapmotion.com/unity)

## Important

You should first create a folder in the root folder, and name it as `DataOutput`. All game data files will be saved in this subfolder, but is not updated by Github.

----

- This project is under [GNU GENERAL PUBLIC LICENSE](https://www.gnu.org/licenses/), please check it in the root folder.
