﻿using UnityEngine;
using System.Collections;
/************************************************************************/
/* Weapon class                                                                     */
/************************************************************************/
public class Weapon : MonoBehaviour {

    public GameObject bullet = null;        // the bullet which this weapon uses
    public float freezeTime = 1f;           // freeze time to let weapon fires in a particular speed
    public float num = 1f;                  // ammo number


    private float lastShootTime = 0f;       // the time elapse after last fire action
    private GameObject fireParticle = null; // fire special effect using this weapon
    // Use this for initialization
    void Start()
    {
        lastShootTime = freezeTime;         // let the weapon can fire immediately
        if (!bullet)
        {
            bullet = gameObject;
        }
        StopFire();
    }

    // Update is called once per frame
    public void Update()
    {
        // increase last fire time to provide freeze function
        if (lastShootTime < freezeTime)
        {
            lastShootTime += Time.deltaTime;
        }
    }
    // fire the bullet 
    public void Fire(Vector3 dir, Vector3 bulletPos)
    {
        if (lastShootTime < freezeTime) // if in frozen statues then return
            return;
        if (num <= 0)                   // make sure it has ammo                
            return;

        // instantiate a bullet with particular position and direction
        GameObject.Instantiate(bullet, bulletPos, Quaternion.FromToRotation(Vector3.forward, dir.normalized));
        num--;
        lastShootTime = 0f;
        if (fireParticle)
            fireParticle.SetActive(true);
    }
    public void StopFire()
    {
        if (fireParticle)
            fireParticle.SetActive(false);
    }
}
