﻿using UnityEngine;
//using UnityEditor;
using System.Collections;
//using UnityEditor.Animations;
/************************************************************************/
/* Use leap motion to pick up items appear in game scene or hand controller called menu                                                                    */
/************************************************************************/
public class PickableItem : MonoBehaviour
{
    [SerializeField]
    private GameObject _prefabObject;                   // the pickable prefab
    [SerializeField]
    private Leap.Unity.PinchDetector[] _pinchDetectors; // the leap motion pinch detector
    [SerializeField]
    //private AnimatorController _anmController;          // use particular animation during piking up

    private GameObject _this = null;                    // instantiated pickable item

    public void ActiveMe()
    {
        if (!_this)
        {
            _this = (GameObject)Instantiate(_prefabObject, transform.position, Quaternion.identity);
            _this.transform.parent = transform;
            _this.transform.position = transform.position;
            _this.transform.rotation = transform.rotation;
            Animator anim = _this.GetComponent<Animator>();
            Debug.Assert(anim);
            //anim.runtimeAnimatorController = _anmController;
            Debug.Log(_this);
        }
        else
            _this.SetActive(true);
    }
    public void DisableMe()
    {
        if (!_this)
            return;
        _this.SetActive(false);
    }
    // called when right hand pinch behavior happens, return a copy of prefab, and disable this item as it's picked up
    public Transform PickMeUp(Vector3 PickPos)
    {
        GameObject cloneOne = (GameObject)Instantiate(_prefabObject, PickPos, Quaternion.identity);
        cloneOne.transform.position = PickPos;
        Animator anim = cloneOne.GetComponent<Animator>();
        Debug.Assert(anim);
        //anim.runtimeAnimatorController = _anmController;
        Debug.Log(_this);
        DisableMe();
        return cloneOne.transform;
    }


    // Use this for initialization
    void Start()
    {
        //ActiveMe();
    }

    // Update is called once per frame
    void Update()
    {

    }
}
