﻿using UnityEngine;
using System.Collections;
using Leap.Unity;
using System.Collections.Generic;

public class TestSelection : MonoBehaviour {

    [SerializeField]
    protected PinchDetector _rightHandPinch;

    [SerializeField]
    protected List<Transform> _objects;
    [SerializeField]
    protected int _randomRange = 20;
    [SerializeField]
    protected float _pickScreenDist = 1.0f;


    protected Transform _currentPinchObj;
    protected Vector3 _startPos;

    [SerializeField]
    protected Transform _cam;
    protected Transform _projectPlaneInCamera;

    protected List<Transform> _allObjects;
	// Use this for initialization
	void Start () {
        _projectPlaneInCamera = _cam.FindChild("ProjectPlane");
        Debug.Assert(_projectPlaneInCamera);
        RandomBirthObjects();
	}

    protected Transform InitPinchObj(Vector3 pos)
    {
        //_currentPinchObj = (Transform)Instantiate(_objects[0], pos, Quaternion.identity);
        return _currentPinchObj;
    }

    protected Vector3 ProjectWorldPos(Vector3 pos)
    {
        //  [6/8/2016 Han]
        return pos;


        Ray ray = new Ray(pos, pos - _cam.position);
        float rayDistance;
        Plane p = new Plane(_projectPlaneInCamera.position - _cam.position, _projectPlaneInCamera.position);
        if (p.Raycast(ray, out rayDistance))
            pos = ray.GetPoint(rayDistance);
        return pos;
    }

    protected void AdjustPinchObj(Vector3 curPos)
    {
        Debug.Assert(_currentPinchObj);

        curPos = ProjectWorldPos(curPos);

        Vector3 size = curPos - _startPos;
        _currentPinchObj.localScale = new Vector3(Mathf.Abs(size.x), Mathf.Abs(size.y), 1);
        Vector3 pos = (curPos + _startPos) / 2.0f;
        _currentPinchObj.position = new Vector3(pos.x, pos.y, 1.0f);
    }

    protected void RandomBirthObjects()
    {
        _allObjects = new List<Transform>();
        for (int i = 0; i < _objects.Count; i++)
        {
            for (int j = 0; j < 5; j++)
            {
                Vector3 pos = new Vector3(_cam.position.x + Random.Range(-_randomRange, _randomRange)
                    , _cam.position.y + Random.Range(-_randomRange, _randomRange)
                    , _cam.position.z + _randomRange + Random.Range(-_randomRange, _randomRange));
                GameObject obj = (GameObject)Instantiate(_objects[i].gameObject, pos, Quaternion.identity);
                obj.GetComponent<Renderer>().material.color = new Color(Random.Range(0, 255)/255.0f, Random.Range(0, 255)/255.0f, Random.Range(0, 255)/255.0f);
                _allObjects.Add(obj.transform);
            }
        }
    }

    protected void SelectObjects(Vector3 endPos)
    {
        //RaycastHit[] hits;
        //Vector3 size = endPos - _startPos;
        //size = new Vector3(Mathf.Abs(size.x), Mathf.Abs(size.y), 1);
        //Vector3 dir = _cam.TransformDirection(0.0f, 0.0f, -1.0f);
        //hits = Physics.BoxCastAll((_startPos+endPos)/2.0f, size/2.0f, dir);
        //Debug.Log(hits.Length);
        //for (int i = 0; i < hits.Length; i++)
        //{
        //    Debug.Log(hits[i].transform.gameObject.tag);
        //    //if (hits[i].collider.tag != "Node")
        //    //{
        //    //    continue;
        //    //}
        //    Renderer rend = hits[i].collider.GetComponent<Renderer>();
        //    if (rend)
        //    {
        //        rend.material.color
        //         = new Color(1.0f, 0.0f, 0.0f);
        //    }
           
        //    Debug.Log(hits[i].transform.name);
        //}

        //////////////////////////////////////////////////////////////////////////
        Camera cam = _cam.GetComponent<Camera>();
        Vector3 x = cam.WorldToScreenPoint(_startPos);
        Vector3 y = cam.WorldToScreenPoint(endPos);
        Vector2 size = y - x;
        size = new Vector2(Mathf.Abs(size.x), Mathf.Abs(size.y));
        Vector2 pointS = (x + y) / 2.0f;
                var rect = new Rect(pointS, size);


        if (rect.Contains(Input.mousePosition))
            Debug.Log("Inside");
        for (int i = 0; i < _allObjects.Count; i++ )
        {
            Vector3 p = cam.WorldToScreenPoint(_allObjects[i].position);
            if (rect.Contains(p))
            {
                Renderer rend = _allObjects[i].GetComponent<Renderer>();
                if (rend)
                {
                    rend.material.color
                     = new Color(1.0f, 0.0f, 0.0f);
                }

                Debug.Log(_allObjects[i].name);

            }
        }
    }

	// Update is called once per frame
	void Update () {
        if (_rightHandPinch.DidStartPinch)
        {
            InitPinchObj(_rightHandPinch.Position);
            _startPos = ProjectWorldPos(_rightHandPinch.Position);
            Debug.Log("StartPoint:" + _startPos);
        }
        else if (_rightHandPinch.IsPinching)
        {
            //Debug.Log("StartPoint:" + _startPos);
            //Debug.Log("Current:" + _rightHandPinch.Position);
            //AdjustPinchObj(_rightHandPinch.Position);
        }
        else if (_rightHandPinch.DidEndPinch)
        {
            Debug.Log("End pinch");
            SelectObjects(_rightHandPinch.Position);
        }
        
        
	
	}
}
