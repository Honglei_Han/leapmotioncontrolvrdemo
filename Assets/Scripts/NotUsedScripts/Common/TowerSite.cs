﻿using UnityEngine;
using System.Collections;

public class TowerSite : MonoBehaviour {

    [SerializeField]
    protected Material m_SelectMat;
    [SerializeField]
    protected Material m_MoveoverMat;
    [SerializeField]
    protected Material m_DefaultMat;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
    void OnMouseDown()
    {
        Renderer rend = GetComponent<Renderer>();
        rend.material = m_SelectMat;
        
    }


}
