﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
/************************************************************************/
/* Control some global behaviors
*/
/************************************************************************/
public class LevelManager : MonoBehaviour
{
    [SerializeField]
    private int _currentLevel = 1;  // Current level
    [SerializeField]
    private List<Wave> _waves;      // Enemy waves in a particular level
    [SerializeField]
    private Transform _birthPlace;  // The start place of enemies
    [SerializeField]
    private Transform _destPlace;   // The destination place of enemies
    public int killEnemyNum = 0;    // Some specific game information
    public int lostEnemys = 0;

    private bool _birthing = false; // The enemy wave generating is in process
    private int _currentWave = 0;   // Current wave ID
    private Text gameInfo;          // UI item to demonstrate game information
    private Player player;          // The player pointer

    // Call when thumb up, or use keyboard
    public void StartAWave()
    {
        if (_birthing)              // cannot birth multiply enemies in the same time
            return;
        _birthing = true;
        if (_currentWave < _waves.Count)    // using coroutine method to birth enemies by BirthEnemies function
        {
            StartCoroutine(BirthEnemies());
        }
    }
    // Start to birth enemies
    // Called by StartAWave
    // using yield method to make sure birth them one by one
    public IEnumerator BirthEnemies()
    {
        Wave cw = _waves[_currentWave];
        // instantiate a new wave
        Instantiate(cw._effect, cw._birthPos ? cw._birthPos.position : _birthPlace.position, Quaternion.identity);
        for (int i = 0; i < cw.Enemies.Count; i++)  // instantiate enemies in this wave one by one
        {
            yield return new WaitForSeconds(cw.Enemies[i].birthWaitTime);   // yield some time to birth a new enemy
            EnemyBehavior emy = (EnemyBehavior)Instantiate(cw.Enemies[i], cw._birthPos ? cw._birthPos.position : _birthPlace.position, Quaternion.identity);
            emy.destPoint = cw.DestPos ? cw.DestPos : _destPlace;
            emy.Init();                 // use enemy's own initialize function
            Debug.Log("Birth" + emy);
        }
        _currentWave++;
        _birthing = false;
   
    }

    // Use this for initialization
    void Start() {
        Random.InitState(System.Environment.TickCount); // get a random seed
        player = GameObject.FindGameObjectWithTag(Tags.player).GetComponent<Player>();  // find player object
        Debug.Assert(player);
        gameInfo = GameObject.Find("Text").GetComponent<Text>();    // find UI object

	}

    // Update is called once per frame
    void Update()
    {
        if (Input.GetButtonDown("Submit"))   // use Return key to start a new wave
        {
            StartAWave();
        }
        Weapon w = player.weapons[player.weaponID].GetComponent<Weapon>();
        gameInfo.text = "Kill enemies:" + killEnemyNum + "\n" + "Lost enemies:" + lostEnemys +"\n"
    + "Weapon:" + w.bullet.GetComponent<Bullet>().bulletType.ToString() + "\n" + "Ammo:" + w.num;
        if (Input.GetButtonDown("Cancel"))
        {
            Application.Quit();
            Debug.Log("Quit");
        }
    }
    [System.Serializable]   // use this to let this class can be configuration in editor
    public class Wave
    {
        [SerializeField]
        private List<EnemyBehavior> _enemies;   // all enemies in this wave
        [SerializeField]
        private Transform _destPos;             // this wave  destination
        [SerializeField]
        public Transform _birthPos;             // this wave  birth position

        public GameObject _effect;              // wave start effect if grabbed

        public List<EnemyBehavior> Enemies      // open the interface
        {
            get
            {
                return _enemies;
            }
        }
        public Transform DestPos
        {
            get
            {
                return _destPos;
            }
        }
    }

}
