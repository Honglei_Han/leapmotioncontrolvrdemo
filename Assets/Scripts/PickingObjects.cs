﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
/************************************************************************/
/* Use this script to right hand controller
 * When hand is pinching some particular items can be picked up                                                                     */
/************************************************************************/
public class PickingObjects : MonoBehaviour
{
    [SerializeField]
    protected List<SphereCollider> _interestedObjects;  // candidate pickable items
    [SerializeField]
    protected Leap.Unity.PinchDetector _PD;             // the pinch detector

    protected Transform _pickingObject;                 // the object is picking up

    // automatically called when pinch behavior happens
    public void StartPick()
    {
        int index = -1;
        float dist = float.MaxValue;
        for (int i = 0; i < _interestedObjects.Count; i++) // the nearest and whose boundary includes pinch position will be picked up
        {
            float d = (_interestedObjects[i].transform.position - _PD.Position).magnitude;
            if (d < _interestedObjects[i].radius)
            {
                if (d < dist)
                {
                    index = i;
                    dist = d;
                }
            }
        }
        if (index != -1)    // get the picked item, and let it moving along with pinch point
        {
            _pickingObject = _interestedObjects[index].GetComponent<PickableItem>().PickMeUp(_PD.Position);
        }
    }
    // release the picked object 
    public void DropPickedObject()
    {
        _pickingObject = null;

    }

    // Use this for initialization
    void Start()
    {

    }

    void FixedUpdate()
    {
        // let the picking object moving along with pinch point 
        if (_pickingObject & _PD.IsPinching)
        {
            _pickingObject.position = _PD.Position;
            //_pickingObject.rotation = _PD.Rotation;
        }
    }
}
