﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Leap;
using UnityEngine.UI;

public class PlayerFire : MonoBehaviour
{
    public GameObject bullet;
    [SerializeField]
    public Leap.Unity.ExtendedFingerDetector handFinger;

    private Text gameInfo;          // UI item to demonstrate game information

    public int killed = 0;

    public void Fire()
    {
        Hand hand;
        hand = handFinger.HandModel.GetLeapHand();

        float[] f = hand.PalmNormal.ToFloatArray();
        Vector3 handNormal = new Vector3(f[0], f[1], f[2]);
        float[] p = hand.PalmPosition.ToFloatArray();
        Vector3 handPos = new Vector3(p[0], p[1], p[2]);
        GameObject.Instantiate(bullet, handPos, Quaternion.FromToRotation(Vector3.forward, handNormal.normalized));
    }

    // Use this for initialization
    void Start()
    {
        gameInfo = GameObject.Find("Text").GetComponent<Text>();    // find UI object

    }

    // Update is called once per frame
    void Update()
    {
        gameInfo.text = "Kill enemies:" + killed + "\n" + "Lost enemies:" + "\n"
    + "Weapon:" + "\n" + "Ammo:" ;
    }
}
