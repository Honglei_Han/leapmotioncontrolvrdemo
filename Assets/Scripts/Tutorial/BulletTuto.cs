﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletTuto : MonoBehaviour
{
    public float speed = 10.0f;
    private Rigidbody rb;                               // rigid body component of this bullet

    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Enemy")
        {
            EnemyAI eai = collision.gameObject.GetComponent<EnemyAI>();
            eai.ShootMe();
        }        
    }

    void FixedUpdate()
    {
        rb.MovePosition(transform.position + transform.forward * speed * Time.deltaTime);

    }
    // Use this for initialization
    void Start()
    {
        rb = GetComponent<Rigidbody>();

    }

    // Update is called once per frame
    void Update()
    {

    }
}
