﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyAI : MonoBehaviour {
    public Transform destPoint;                             // destination
    private NavMeshAgent nav;
    public PlayerFire p;

    public void ShootMe()
    {
        p.killed++;
        Destroy(gameObject);
    }

    void Awake()
    {
        nav = GetComponent<UnityEngine.AI.NavMeshAgent>();  // get nav agent component
        if (!nav)
            return;
        Init();
    }
    public void Init()
    {
        nav.SetDestination(destPoint.position);     // set nav destination
    }

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
