﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class GameDataRecorder : MonoBehaviour
{
    private List<StreamWriter> _recordFile;

    // Use this for initialization
    void Start()
    {
        _recordFile = new List<StreamWriter>();
        _recordFile.Add(File.CreateText("DataOutput//PlayerRecord" + Random.value));
        _recordFile.Add(File.CreateText("DataOutput//CameraRecord" + Random.value));
    }

    // Update is called once per frame
    void Update()
    {

    }
    void OnApplicationQuit()
    {
        for (int i = 0; i < _recordFile.Count; i++)
        {
            _recordFile[i].Close();
        }
    }
    public StreamWriter playerFile      // open the interface
    {
        get
        {
            return _recordFile[0];
        }
    }
    public StreamWriter cameraFile
    {
        get
        {
            return _recordFile[1];
        }

    }

}
