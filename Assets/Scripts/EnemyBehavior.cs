﻿using UnityEngine;
using System.Collections;
/************************************************************************/
/* Enemies will keep moving in a nav path until dead or arrive to the destination                                                                     */
/************************************************************************/
public class EnemyBehavior : MonoBehaviour
{
    //public float patrolSpeed = 2f;                          // The nav mesh agent's speed when patrolling.
    //public Transform patrolWayPoints;                       // An array of transforms for the patrol route.
    //public float patrolWaitTime = 1f;                       // The amount of time to wait when the patrol way point is reached.
    public Transform destPoint;                             // destination
    public float birthWaitTime = 1.0f;                      // birth delay

    private UnityEngine.AI.NavMeshAgent nav;                // Reference to the nav mesh agent.
    //private float patrolTimer;                              // A timer for the patrolWaitTime.

    public float health = 100.0f;                           // enemy health
    public GameObject disappearEffect = null;               // special effect when enemy died if grabbed
    public float effectDurationTime = 1f;                   // the die special effect duration time

    private LevelManager lm;                                // pointer to game level manager

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        //Patrolling();
    }
    void Awake()
    {
        // get level manager
        lm = GameObject.FindWithTag(Tags.gameController).GetComponent<LevelManager>();
        Debug.Assert(lm);
        nav = GetComponent<UnityEngine.AI.NavMeshAgent>();  // get nav agent component
        if (!nav || !destPoint)
            return;
        Init();
    }
    public void Init()
    {
        nav.SetDestination(destPoint.position);     // set nav destination
    }
  
    // Called by bullet, cause the damage of this enemy
    public void Damage(float damage)
    {
        Debug.Log(name + damage);
        health -= damage;
        if (health <= 0)
        {
            health = 0f;
            Die();
        }
    }
    protected void Die()
    {
        if (disappearEffect)
        {
            GameObject g = (GameObject)GameObject.Instantiate(disappearEffect, transform.position, Quaternion.identity);
            Destroy(g, effectDurationTime);
        }
        GameObject.Destroy(gameObject);
        lm.killEnemyNum++;
    }
}
