﻿using UnityEngine;
using System.Collections;

public class Tags : MonoBehaviour
{
    // A list of tag strings.
    public const string player = "Player";
    public const string gameController = "GameManager";
    public const string enemy = "Enemy";
    public const string obstacle = "Obstacle";
    public const string item = "Item";
    public const string terrain = "Terrain";

}
