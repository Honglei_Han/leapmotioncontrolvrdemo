﻿using UnityEngine;
using System.Collections;
using Leap;

/************************************************************************/
/* Use Leap motion hand controller to interact with game scene
 * Can also use mouse and keyboard */
/************************************************************************/
public class LMController : MonoBehaviour
{
    [Tooltip("Scene rotate speed using palm")]  // use this to pop out tool tip when mouse staying
    public float _viewRotSpeed = 10.0F;         // camera rotate and zoom speed
    public float _viewMoveSpeed = 5.0f;
    public Transform _transCam;                 // pointer to main camera
    [SerializeField]
    private float _distance = 50.0f;
    [SerializeField]
    private float _height = 40.0f;
    [Tooltip("Palms detectors")]                // use this to pop out tool tip when mouse staying
    [SerializeField]
    private Leap.Unity.PalmDirectionDetector[] _PalmDetectors;  // get hand palm direction

    private Vector3 _startDir;                  // record palm's direction
    private bool _zooming = false;              // camera is zooming?
    private bool _zoomIn = true;                // zoom in or out?
    private float _xDeg, _yDeg;                  // record camera target, this game object, rotation

    private GameDataRecorder dataRecorder;  // the script manages all data files

    //////////////////////////////////////////////////////////////////////////
    // automatically called by leap motion hand controller, when palm point to right
    public void BeginRotateSceneHoz()
    {
        Debug.Log("Begin Rotate");
        Hand hand;

        hand = _PalmDetectors[0].HandModel.GetLeapHand();
        float[] f = hand.PalmNormal.ToFloatArray();
        _startDir = new Vector3(f[0], f[1], f[2]);
    }
    // end of sliding, apply camera rotation according to palm slide direction
    public void EndRotateSceneHoz()
    {
        Debug.Log("End Rotate");
        Hand hand;
        Vector3 normal;
        hand = _PalmDetectors[0].HandModel.GetLeapHand();
        float[] f = hand.PalmNormal.ToFloatArray();
        normal = new Vector3(f[0], f[1], f[2]);
        Vector3 endDir = normal;

        Vector3 pointDir = endDir.normalized - _startDir.normalized;

        // apply rotation
        _xDeg += pointDir.z > 0 ? _viewRotSpeed : -_viewRotSpeed;   // determine slide right or left
        Quaternion fromRotation = transform.rotation;
        Quaternion toRotation = Quaternion.Euler(_yDeg, _xDeg, 0);
        transform.rotation = Quaternion.Lerp(fromRotation, toRotation, 1);
        Debug.Log("StarDir" + _startDir.normalized+"EndDir"+endDir.normalized);
    }
    // rotation in vertical 
    public void BeginRotateSceneVel()
    {
        Debug.Log("Begin Rotate Vel");
        Hand hand;

        hand = _PalmDetectors[1].HandModel.GetLeapHand();
        float[] f = hand.PalmNormal.ToFloatArray();
        _startDir = new Vector3(f[0], f[1], f[2]);
    }
    public void EndRotateSceneVel()
    {
        Debug.Log("End Rotate Vel");
        Hand hand;
        Vector3 normal;
        hand = _PalmDetectors[1].HandModel.GetLeapHand();
        float[] f = hand.PalmNormal.ToFloatArray();
        normal = new Vector3(f[0], f[1], f[2]);
        Vector3 endDir = normal;

        Vector3 pointDir = endDir.normalized - _startDir.normalized;
        _yDeg += pointDir.z < 0 ? _viewRotSpeed : -_viewRotSpeed;   // determine slide up or down
        Quaternion fromRotation = transform.rotation;
        Quaternion toRotation = Quaternion.Euler(_yDeg, _xDeg, 0);
        transform.rotation = Quaternion.Lerp(fromRotation, toRotation, 1);
        Debug.Log("StarDir" + _startDir.normalized + "EndDir" + endDir.normalized);
    }
    // zoom in and out functions directly called by leap motion controller using logic
    public void ZoomIn()
    {
        _zooming = true;
        _zoomIn = true;
    }
    public void StopZoom()
    {
        _zooming = false;
    }
    public void ZoomOut()
    {
        _zooming = true;
        _zoomIn = false;
    }
    // Use this for initialization
    void Start()
    {
        _xDeg = transform.rotation.eulerAngles.y;
        _yDeg = transform.rotation.eulerAngles.x;

        // Get the game data record file from game manager [10/23/2016 Han]
        dataRecorder = GameObject.FindWithTag(Tags.gameController).GetComponent<GameDataRecorder>();
        Debug.Assert(dataRecorder);
    }
    void FixedUpdate()
    {
        // use mouse to rotate the camera look object, then the camera will change along with it [9/15/2016 Han]
        _xDeg += Input.GetAxis("Horizontal") * _viewRotSpeed * Time.deltaTime;
        _yDeg += Input.GetAxis("Vertical") * _viewRotSpeed * Time.deltaTime;
        Quaternion fromRotation = transform.rotation;
        Quaternion toRotation = Quaternion.Euler(_yDeg, _xDeg, 0);
        transform.rotation = Quaternion.Lerp(fromRotation, toRotation, 1);

        // use keyboard to zoom in and out
        if (Input.GetKey(KeyCode.Z))
        {
            _zooming = true;
            _zoomIn = true;
        }
        if (Input.GetKeyUp(KeyCode.Z))
        {
            _zooming = false;
        }
        if (Input.GetKey(KeyCode.X))
        {
            _zooming = true;
            _zoomIn = false;
        }
        if (Input.GetKeyUp(KeyCode.X))
        {
            _zooming = false;
        }       // use leap motion to zoom
        if (_zooming)
        {
            float z = _zoomIn ? -_viewMoveSpeed : _viewMoveSpeed;
            float ratio = _height/_distance;
            _distance += z * Time.deltaTime;
            _height = _distance * ratio;
            dataRecorder.cameraFile.Write(Time.time+"\tPlayer is zooming\n");
        }
        // calculate this object's position in target's local coordination
        Vector3 targetPos = transform.TransformPoint(0, _height, -_distance);
        _transCam.position = Vector3.Lerp(_transCam.position, targetPos, _viewMoveSpeed * Time.deltaTime);
        _transCam.LookAt(transform);

        // write current camera position and rotation to the file
        dataRecorder.cameraFile.Write(Time.time.ToString() + "\tCurrent camera position is:\t" + Camera.main.transform.position + "\tCurrent camera rotation is:\t" + Camera.main.transform.rotation.eulerAngles + "\n");

    }
}
