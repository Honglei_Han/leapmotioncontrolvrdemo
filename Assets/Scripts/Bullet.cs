﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
/************************************************************************/
/* Bullet behavior                                                                     */
/************************************************************************/

// indicate bullet's type
// Default
// Pistol: basic weapon
// Gun: more powerful with higher speed and especially duration damage
// Rocket: very powerful with explosion effect
// Laser: powerful with penetrate effect
public enum WeaponType { Default, Pistol, Gun, Rocket, Laser };

// The GameObject requires a Rigidbody component
[RequireComponent(typeof(Rigidbody))]
public class Bullet : MonoBehaviour
{

    private Rigidbody rb;                               // rigid body component of this bullet
    private bool bMove = true;                          // indicate whether the bullet is still moving along with it's trajectory

    public float speed = 50.0f;                         // the moving speed
    public float damage = 5.0f;                         // the basic damage to enemies caused by the bullet
    public GameObject explosionEffect = null;           // explosion effect if grabbed
    public float effectDurationTime = 5.0f;             // the effect duration time
    public float damageRadius = 10.0f;                  // explosion bullet damage effect radius
    public WeaponType bulletType = WeaponType.Default;
    public float damagePerSecond = 0.0f;                // duration damage 

    // Use this for initialization
    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    public void FixedUpdate()
    {
        if (bMove)  // keep moving
            rb.MovePosition(transform.position + transform.forward * speed * Time.deltaTime);
    }
    // explosion effect called when bullet collide with some objects 
    void Explosion(Collider other)
    {
        if (!explosionEffect)
            return;
        GameObject g = (GameObject)GameObject.Instantiate(explosionEffect, transform.position, transform.rotation);
        Debug.Log(transform.position);
        switch (bulletType)
        {
            case WeaponType.Pistol:
            case WeaponType.Gun:
            case WeaponType.Laser:
                g.transform.SetParent(other.transform);
                break;
            case WeaponType.Rocket:
            default:
                break;
        }
        Destroy(g, effectDurationTime);
    }
    // the bullet hit some objects
    public void OnTriggerEnter(Collider other)
    {
        switch (other.tag)
        {
            case Tags.obstacle:
            case Tags.terrain:
                bMove = false;
                break;
            case Tags.enemy:
                break;
            default:
                return;
        }
        StartCoroutine("BeginDamage", other);   // use coroutine call method to make sure some persistent damage effect
    }
    // coroutine function called when the bullet collider with some objects
    private IEnumerator BeginDamage(Collider other)
    {
        GetComponent<Renderer>().enabled = false;

        EnemyBehavior enemy = other.GetComponent<EnemyBehavior>();
        Explosion(other);
        switch (bulletType)
        {
            case WeaponType.Pistol:
                if (!enemy)
                    break;
                enemy.Damage(damage);
                break;
            case WeaponType.Gun:
                //Debug.Log("Persistent");
                bMove = false;
                if (!enemy)
                    break;
                transform.SetParent(other.transform);
                enemy.Damage(damage);
                float t = 0.0f;
                while (t < effectDurationTime)  // implement persistent damage effect
                {
                    t += Time.deltaTime;
                    if (enemy)
                        enemy.Damage(damagePerSecond * Time.deltaTime);
                    else
                        break;
                    yield return null;
                }
                break;
            case WeaponType.Rocket: // objects in a 
                bMove = false;
                // implement explosion damage effect
                // OverlapSphere function should always use the last parameter to make sure overlap with trigger objects
                Collider[] hitColliders = Physics.OverlapSphere(transform.position, damageRadius, Physics.AllLayers, QueryTriggerInteraction.Collide);
                List<EnemyBehavior> damagedActors = new List<EnemyBehavior>();
                Debug.Log("overlap" + hitColliders.GetLength(0));
                foreach (Collider col in hitColliders)
                {
                    EnemyBehavior a = col.gameObject.GetComponent<EnemyBehavior>();
                    Debug.Log(col.gameObject.name);
                    if (a && !damagedActors.Contains(a))
                    {
                        float r = Vector3.Distance(a.transform.position, transform.position);
                        r = (damageRadius - r) / damageRadius;
                        r = r < 0f ? 0 : r;
                        a.Damage(damage * r);
                        damagedActors.Add(a);
                        Debug.Log("Damage" + a + r);
                    }
                }
                break;
            case WeaponType.Laser:  // if it's laser type, it wouldn't stop moving forward
                if (!enemy)
                    break;
                enemy.Damage(damage);
                break;
            default:
                break;
        }
        if (!bMove) // destroy dead bullet
            GameObject.Destroy(gameObject);
    }

}
