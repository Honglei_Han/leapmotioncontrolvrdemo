﻿using UnityEngine;
using System.Collections;
/************************************************************************/
/* Interactive behaviors the player can do                                                                     */
/************************************************************************/
public class Player : MonoBehaviour
{
    public Weapon[] weapons;    // weapons the player can use
    public int weaponID = 0;    // current using weapon ID in the weapon list

    private GameDataRecorder dataRecorder;  // the script manages all data files

    // Use this for initialization
    void Start()
    {
        // instantiate all weapons based on their prefabs, then inactive them except the current using one
        for (int i = 0; i < weapons.Length; i++)
        {
            weapons[i] = (Weapon)Instantiate(weapons[i], transform.position, Quaternion.identity);
            weapons[i].gameObject.SetActive(i == weaponID);
        }
        // Get the game data record file from game manager [10/23/2016 Han]
        dataRecorder = GameObject.FindWithTag(Tags.gameController).GetComponent<GameDataRecorder>();
        Debug.Assert(dataRecorder);

    }
    // Update is called once per frame
    void Update()
    {
        if (Input.GetButton("Fire1"))
        {
            if (weaponID > -1)
            {
                // fire the bullet from camera through the direction that mouse arrow points
                Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                RaycastHit[] hits;
                hits = Physics.RaycastAll(ray);
                for (int i = 0; i < hits.Length; i++)
                {
                    RaycastHit hit = hits[i];
                    if (hit.transform.tag == Tags.terrain)
                    {
                        Vector3 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                        Vector3 dir = hit.point - mousePos;
                        Fire(dir, mousePos);
                    }
                }
            }
        }
        // change current weapon using mouse wheel
        float f = Input.GetAxis("Mouse ScrollWheel");
        if (f > 0)
            NextWeapon(1);
        else if (f < 0)
            NextWeapon(-1);
    }
    // fire the bullet from pos through dir direction
    public void Fire(Vector3 dir, Vector3 pos)
    {
        if (weapons.Length > 0)
        {
            weapons[weaponID].Fire(dir, pos);
        }
        // Record fire information [10/23/2016 Han]
        dataRecorder.playerFile.Write(Time.time + "\tPlayer fire" + "\tusing weapon:\t" + weapons[weaponID] + "\n");
    }
    // stop firing
    public void StopFire()
    {
        if (weapons.Length > 0)
        {
            weapons[weaponID].SendMessage("StopFire");
        }
    }
    // change current weapon to the next weapon
    bool NextWeapon(int next)
    {
        // Record changing weapon information [10/23/2016 Han]
        dataRecorder.playerFile.Write(Time.time + "\tChange weapon from\t" + weapons[weaponID]);
        weapons[weaponID].gameObject.SetActive(false);
        weaponID = Mathf.Abs(weaponID + next + weapons.Length) % weapons.Length;

        weapons[weaponID].gameObject.SetActive(true);
        StopFire(); // new weapon should in stop firing status
        // Record changing weapon information [10/23/2016 Han]
        dataRecorder.playerFile.Write("\tto:\t" + weapons[weaponID] + "\n");
        return true;
    }
}
