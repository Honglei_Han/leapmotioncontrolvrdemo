﻿using UnityEngine;
using System.Collections;
/************************************************************************/
/* To collect the enemies and notify game manager                                                                     */
/************************************************************************/
public class EndPoint : MonoBehaviour
{
    private LevelManager lm;

    // Use this for initialization
    void Start()
    {
        lm = GameObject.FindGameObjectWithTag(Tags.gameController).GetComponent<LevelManager>();
    }

    // Update is called once per frame
    void Update()
    {

    }
    void OnTriggerEnter(Collider other)
    {
        if (other.tag == Tags.enemy)
        {
            GameObject.Destroy(other.gameObject);
            lm.lostEnemys++;
        }
    }
}
